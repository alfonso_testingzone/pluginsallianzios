//
//  ScannerInterface.swift
//  AllianzDocs
//
//  Created by Rodolfo Castillo on 3/16/17.
//  Copyright © 2017 Mariachi.io. All rights reserved.
//

import Foundation
import UIKit

let screenSize: CGRect = UIScreen.main.bounds
let allianzeBlue = UIColor(red: 95/255, green: 132/255, blue: 199/255, alpha: 1)
let activeBlue = UIColor(red: 33/255, green: 156/255, blue: 232/255, alpha: 1)



class ScannerInterface {
    
    let credentialScanner = iScanner()
    let documentScanner = iScannerLG()
    var viewController: UIViewController!
    
    
    
    init(forViewController viewController: UIViewController) {
        self.viewController = viewController
    }
    
    func showScanner(forCredential:Bool){
        if forCredential {
            DispatchQueue.main.async {
                self.viewController.present(self.credentialScanner, animated: true, completion: nil)
            }
        } else {
            DispatchQueue.main.async {
                self.viewController.present(self.documentScanner, animated: true, completion: nil)
            }
        }
    }
    
}
