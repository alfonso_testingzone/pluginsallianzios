@objc(BlinkPlugin) class BlinkPlugin : CDVPlugin {
  @objc(blinkMethod:)
  func blinkMethod(_ command: CDVInvokedUrlCommand) {
    var pluginResult = CDVPluginResult(
      status: CDVCommandStatus_ERROR
    )

    let msg = command.arguments[0] as? String ?? ""

    if msg.characters.count > 0 {
      let toastController: blinkViewController =
        blinkViewController()
        toastController.setPluginDelegate(forPluggin: self, andCommand: command)

      self.viewController?.present(
        toastController,
        animated: true,
        completion: nil
      )

      //DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
      //  toastController.dismiss(
      //      animated: true,
      //      completion: nil
      //  )
      //}
    }


  }
}
