package io.mariachi.allianzdocs.ui;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.MediaRouteButton;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.util.Calendar;

import io.mariachi.allianzdocs.camera.view.CameraView;
import io.mariachi.allianzdocs.utils.Utils;

public class Docs extends AppCompatActivity {

    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    private CameraView mCameraView;

    private Handler mBackgroundHandler;
    private boolean flag;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getResources().getIdentifier("activity_docs", "layout", getPackageName()));
        progressBar = (ProgressBar) findViewById(getResources().getIdentifier("CropProgressBar", "id", getPackageName()));
        ImageView imgBack = (ImageView) findViewById(getResources().getIdentifier("backDomicilio", "id", getPackageName()));
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        settupCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Utils.itsFinal) {
            finish();
        }
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);

        if (rc == PackageManager.PERMISSION_GRANTED) {
            mCameraView.start();
        } else {
            requestCameraPermission();
        }
    }

    @Override
    protected void onPause() {
        mCameraView.stop();
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mBackgroundHandler != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                mBackgroundHandler.getLooper().quitSafely();
            } else {
                mBackgroundHandler.getLooper().quit();
            }
            mBackgroundHandler = null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (permissions.length != 1 || grantResults.length != 1) {
//                    throw new RuntimeException("Error on requesting camera permission.");

                }
                if (grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this, "Permiso no aprobado",
                            Toast.LENGTH_SHORT).show();
                }
                // No need to start camera here; it is handled by onResume
                break;
        }
    }


    /**
     * Handles the requesting of the camera permission.  This includes
     * showing a "Snackbar" message of why the permission is needed then
     * sending the request.
     */
    private void requestCameraPermission() {


        final String[] permissions = new String[]{Manifest.permission.CAMERA};

        if (!ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.CAMERA)) {
            ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
            return;
        }

        final Activity thisActivity = this;

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityCompat.requestPermissions(thisActivity, permissions,
                        RC_HANDLE_CAMERA_PERM);
            }
        };

        Snackbar.make(getViewFromActivity(thisActivity), "Access to the camera is needed for detection",
                Snackbar.LENGTH_INDEFINITE)
                .setAction("ok", listener)
                .show();
    }

    public View getViewFromActivity(Activity activity) {
        return (ViewGroup) ((ViewGroup) activity
                .findViewById(getResources().getIdentifier("content", "id", getPackageName()))).getChildAt(0);
    }

    private void settupCamera() {
        mCameraView = (CameraView) findViewById(getResources().getIdentifier("camera", "id", getPackageName()));
        if (mCameraView != null) {
            mCameraView.setFlash(CameraView.FLASH_OFF);
            mCameraView.addCallback(mCallback);

        }
    }


    private Handler getBackgroundHandler() {
        if (mBackgroundHandler == null) {
            HandlerThread thread = new HandlerThread("background");
            thread.start();
            mBackgroundHandler = new Handler(thread.getLooper());
        }
        return mBackgroundHandler;
    }


    /**
     * Callback
     */
    private CameraView.Callback mCallback
            = new CameraView.Callback() {

        @Override
        public void onCameraOpened(CameraView cameraView) {
            log("onCameraOpened");
        }

        @Override
        public void onCameraClosed(CameraView cameraView) {
            log("onCameraClosed");
        }

        @Override
        public void onPictureTaken(CameraView cameraView, final byte[] data) {


            new AsyncTask<Void, Void, Void>() {

                @TargetApi(Build.VERSION_CODES.KITKAT)
                @Override
                protected Void doInBackground(Void... voids) {
                    log("lengh: " + data.length);
                    Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();

                    int orientation = getResources().getConfiguration().orientation;
                    if (orientation != Configuration.ORIENTATION_PORTRAIT) {
                        log("Landscape");
                        try {
                            int realAncho = realImage.getWidth();
                            int realAlto = realImage.getHeight();

                            int casilla = realAlto / 10;

                            int recHeight = (int) (casilla * Utils.constanteX);
                            int recWidth = (int) (casilla * Utils.constanteY);

                            log("alto: " + recHeight);
                            log("realAlto: " + realAlto);

                            int espacioy = (((realAlto - recHeight) / 2));
                            int espaciox = ((realAncho - recWidth) / 2);

                            log("x: " + espaciox);
                            log("y: " + espacioy);

//                            realImage = Bitmap.createBitmap(realImage, espaciox / 2, espacioy * 3, alto, ancho - (ancho / 5));
                            realImage = Bitmap.createBitmap(realImage, espaciox - (espaciox / 2) ,  espacioy - (espacioy / 2), recWidth, recHeight);


                            realImage.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                            byte[] byteArray = stream.toByteArray();
                            Utils.pictureTaken = byteArray;
                            realImage.recycle();
                            realImage = null;


                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        log("Portrait");


                        Matrix matrix = new Matrix();
                        matrix.postRotate(90);
                        realImage = Bitmap.createBitmap(realImage, 0, 0, realImage.getWidth(), realImage.getHeight(), matrix, true);

                        int realAncho = realImage.getWidth();
                        int realAlto = realImage.getHeight();

                        int regx = realAncho / Utils.rejilla; //rejilla de 9
                        int ancho = (int) (regx * Utils.constanteX);
                        int canty = realAlto / regx;
                        int regy = realAlto / canty; // rejilla alto
                        int alto = (int) (regy * Utils.constanteY);

                        int espacioy = ((realAlto - alto) / 2) - Utils.rejilla * 4;
                        int espacioyAlt = (realAlto - alto) / 2;
                        int espaciox = (realAncho - ancho) / 2;


                        realImage = Bitmap.createBitmap(realImage, espaciox - (espaciox / 2), espacioy, ancho, alto);

                        realImage.compress(Bitmap.CompressFormat.JPEG, 90, stream);
                        byte[] byteArray = stream.toByteArray();

                        Utils.pictureTaken = byteArray;

                        realImage.recycle();
                        realImage = null;

                    }


                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    progressBar.setVisibility(View.GONE);
                    flag = false;
                    Intent crop = new Intent(getApplicationContext(), CropImages.class);
                    startActivity(crop);
                }
            }.execute();


        }

        @Override
        public void onPreviewFrame(byte[] data) {
            super.onPreviewFrame(data);

        }
    };

    public void tomarFotoDocs(View v) {
        if (!flag) {
            if (mCameraView != null) {

                progressBar.setVisibility(View.VISIBLE);
                mCameraView.start();
                mCameraView.takePicture();
                flag = true;
            }
        }

    }

    private void log(String content) {
        Log.e("myLog", content);
    }
}
