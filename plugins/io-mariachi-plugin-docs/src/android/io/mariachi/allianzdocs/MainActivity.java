package io.mariachi.allianzdocs;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;


import java.io.ByteArrayOutputStream;

import io.mariachi.allianzdocs.ui.Docs;
import io.mariachi.allianzdocs.ui.INE;
import io.mariachi.allianzdocs.ui.Cards;
import io.mariachi.allianzdocs.utils.Utils;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;

public class MainActivity extends CordovaPlugin {
    public CallbackContext callbackContext;
    boolean cardFlag = false;
    JSONObject cardObj = null;
    private static final int MY_SCAN_REQUEST_CODE = 37;

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
      
      // TODO Auto-generated method stub
      this.callbackContext = callbackContext;
      try {
          if ("docFunctionSm".equals(action)) {
            Intent i = new Intent(this.cordova.getActivity().getApplicationContext(), INE.class);
            cordova.getActivity().startActivity(i);
            PluginResult resultsImg = new PluginResult(PluginResult.Status.NO_RESULT);
            resultsImg.setKeepCallback(true);
            callbackContext.sendPluginResult(resultsImg);
            return true;
          }
          else if ("docFunctionLg".equals(action)) {
            Intent d = new Intent(this.cordova.getActivity().getApplicationContext(), Docs.class);
            cordova.getActivity().startActivity(d);
            PluginResult resultsImg = new PluginResult(PluginResult.Status.NO_RESULT);
            resultsImg.setKeepCallback(true);
            callbackContext.sendPluginResult(resultsImg);
            return true;
          }
          else if ("cardFunction".equals(action)) {
            Intent ca = new Intent(this.cordova.getActivity().getApplicationContext(), Cards.class);
            cordova.getActivity().startActivity(ca);
            PluginResult resultsImg = new PluginResult(PluginResult.Status.NO_RESULT);
            resultsImg.setKeepCallback(true);
            callbackContext.sendPluginResult(resultsImg);
            return true;
          }
          return false;
      } catch (Exception e) {
          e.printStackTrace();
          PluginResult res = new PluginResult(PluginResult.Status.JSON_EXCEPTION);
          callbackContext.sendPluginResult(res);
          return false;
      }
      
  }

  public String base64Transform(Bitmap bitmap) {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
      byte[] byteArray = byteArrayOutputStream.toByteArray();
      String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
      return encoded;
  }


    @Override
  public void onResume(boolean multitasking) {
      super.onResume(multitasking);
        if (Utils.itsFinal) {
            Utils.itsFinal = false;
            String outText = base64Transform(rotarImagen(Utils.pictureFinal));
            PluginResult resultsImg = new PluginResult(PluginResult.Status.OK, outText);
            resultsImg.setKeepCallback(false);
            callbackContext.sendPluginResult(resultsImg);
        }
        
        if (Utils.blink) {
            Utils.blink = false;

        }

        if (Utils.flagCard)
        {
            Utils.flagCard = false;
            PluginResult resultsImg = new PluginResult(PluginResult.Status.OK, Utils.cardInfo);
            resultsImg.setKeepCallback(false);
            callbackContext.sendPluginResult(resultsImg);
        }
        else
        {
          PluginResult resultsImg = new PluginResult(PluginResult.Status.ERROR, "Evento cancelado.");
          callbackContext.sendPluginResult(resultsImg);
        }
    }

    public Bitmap rotarImagen(byte[] data) {
        if (data != null && data.length > 0) {
            Bitmap realImage = BitmapFactory.decodeByteArray(data, 0, data.length);

        /*int w = realImage.getWidth();
        int h = realImage.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(270);

        return Bitmap.createBitmap(realImage, 0, 0, w, h, mtx, true);*/

            return realImage;
        } else {
            return null;
        }
    }

}
