//
//  ViewController.swift
//  swiftCardIo
//
//  Created by Rodolfo Castillo on 3/17/17.
//  Copyright © 2017 Mariachi.io. All rights reserved.
//


import UIKit

class cardScanner: UIViewController, CardIOViewDelegate {
    /// This method will be called if the user cancels the scan. You MUST dismiss paymentViewController.
    /// @param paymentViewController The active CardIOPaymentViewController.
    func setPluginDelegate(forPluggin plgn: AllianzDocs, andCommand command: CDVInvokedUrlCommand){
        self.pluginDelegate = plgn
        self.command = command
    }

    private var _orientations = UIInterfaceOrientationMask.portrait
    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        get { return self._orientations }
        set { self._orientations = newValue }
    }

    var pluginDelegate: AllianzDocs!
    var command: CDVInvokedUrlCommand!

    func cardIOView(_ cardIOView: CardIOView!, didScanCard cardInfo: CardIOCreditCardInfo!) {
        print(cardInfo)

        self.fudgeLabel.text = cardInfo.cardNumber
        self.fudgeDate.text = "\(cardInfo.expiryMonth)/\(cardInfo.expiryYear)"

        self.rice_n = cardInfo.cardNumber
        self.shrimp_dm = "\(cardInfo.expiryMonth)"
        self.shrimp_dy = "\(cardInfo.expiryYear)"

        aCardIOView.removeFromSuperview()
        aCardIOView = CardIOView(frame: self.view.frame)
        aCardIOView.delegate = self
        aCardIOView.hideCardIOLogo = true
        self.view.addSubview(aCardIOView)

        self.view.bringSubview(toFront:aCardIOView)
        self.view.bringSubview(toFront:fudgeTag)
        self.view.bringSubview(toFront:fudgeDateTag)
        self.view.bringSubview(toFront:doneButton)
        self.view.bringSubview(toFront:fudgeLabel)
        self.view.bringSubview(toFront:fudgeDate)
        self.view.bringSubview(toFront:retryButton)
        self.view.bringSubview(toFront:navbar)

    }

    var fudgeTag = UILabel()
    var fudgeDateTag = UILabel()
    var fudgeLabel = UILabel()
    var fudgeDate = UILabel()

    var doneButton: UIButton!
    var retryButton: UIButton!

    var rice_n: String!
    var shrimp_dm: String!
    var shrimp_dy: String!

    var aCardIOView = CardIOView()
   let screen = UIScreen.main.bounds

    func editFudge(){

        var inputAlert = UIAlertController(title: "Edita/Agrega", message: "Edita/Agrega la información de la tarjeta. Introduce el número sin espacios, el mes con número, ejemplo: Enero -> 01. Y escribe el año completo, ejemplo: 2040", preferredStyle: .alert)

        let saveAction = UIAlertAction(title: "Save", style: .default, handler: {
            alert -> Void in

            self.rice_n = (inputAlert.textFields![0] as UITextField).text!
            self.shrimp_dm = (inputAlert.textFields![1] as UITextField).text!
            self.shrimp_dy = (inputAlert.textFields![2] as UITextField).text!

            DispatchQueue.main.async {
                self.fudgeLabel.text = self.rice_n
                self.fudgeDate.text = "\(self.shrimp_dm!)/\(self.shrimp_dy!)"
            }

            self.fudgeLabel.text = self.rice_n
            self.fudgeDate.text = "\(self.shrimp_dm!)/\(self.shrimp_dy!)"


        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {
            (action : UIAlertAction!) -> Void in

        })

        inputAlert.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Número de la tarjeta"

            if self.rice_n != nil {
                textField.text = self.rice_n
            }

        }
        inputAlert.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Mes"

            if self.shrimp_dm != nil && self.shrimp_dm != "0"{
                textField.text = self.shrimp_dm
            }
        }
        inputAlert.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Año"

            if self.shrimp_dy != nil && self.shrimp_dm != "0"{
                textField.text = self.shrimp_dy
            }
        }

        inputAlert.addAction(saveAction)
        inputAlert.addAction(cancelAction)

        DispatchQueue.main.async {
            self.present(inputAlert, animated: true, completion: nil)
        }


    }

    func endProcess(){
        let docsResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: [self.rice_n, self.shrimp_dm, self.shrimp_dy])
        self.pluginDelegate.commandDelegate.send(docsResult, callbackId: self.command.callbackId)
        self.dismiss(animated: true, completion: nil)

    }

    func goAway (){
        let docsResult = CDVPluginResult(status: CDVCommandStatus_OK, messageAs: "El Usuario ha cancelado")
        self.pluginDelegate.commandDelegate.send(docsResult, callbackId: self.command.callbackId)
        self.dismiss(animated: true, completion: nil)

    }

    var navbar: UINavigationBar!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.black
        // Do any additional setup after loading the view, typically from a nib.
        CardIOUtilities.preload()

        navbar = UINavigationBar(frame: CGRect(x:0, y:0, width:UIScreen.main.bounds.size.width,height:65));
        navbar.tintColor = UIColor.white
        navbar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white]
        navbar.barTintColor = UIColor(red: 95/255, green: 132/255, blue: 199/255, alpha: 1)


        let navItem = UINavigationItem(title: "Pago")

        let navBarbutton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.cancel, target: self, action: #selector(goAway))


        navItem.leftBarButtonItem = navBarbutton

        self.doneButton = UIButton(frame: CGRect(x: screen.width - 80 , y: screen.height - 65, width: 80, height: 65))
        self.doneButton.setTitle("Listo", for: .normal)
        self.doneButton.setTitleColor(UIColor.white, for: .normal)

        self.retryButton = UIButton(frame: CGRect(x: 0 , y: screen.height - 65, width: 80, height: 65))
        self.retryButton.setTitle("Editar", for: .normal)
        self.retryButton.setTitleColor(UIColor.white, for: .normal)


        navbar.items = [navItem]

        self.fudgeTag.frame = CGRect(x: 0, y: screen.height - 69, width: 200, height: 15)
        self.fudgeTag.font = UIFont(name: "Helvetica-Bold", size: 10)
        self.fudgeTag.text = "Número de la tarjeta"
        self.fudgeTag.textColor = UIColor.white
        self.fudgeTag.textAlignment = .center
        self.fudgeTag.center.x = screen.width / 2

        self.fudgeLabel.frame = CGRect(x: 0, y: screen.height - 52, width: 200, height: 15)
        self.fudgeLabel.font = UIFont(name: "Helvetica", size: 12)
        self.fudgeLabel.text = ""
        self.fudgeLabel.textColor = UIColor.white
        self.fudgeLabel.textAlignment = .center
        self.fudgeLabel.center.x = screen.width / 2

        self.fudgeDate.frame = CGRect(x: 0, y: screen.height - 18, width: 200, height: 15)
        self.fudgeDate.font = UIFont(name: "Helvetica", size: 12)
        self.fudgeDate.text = ""
        self.fudgeDate.textColor = UIColor.white
        self.fudgeDate.textAlignment = .center
        self.fudgeDate.center.x = screen.width / 2


        self.fudgeDateTag.frame = CGRect(x: 0, y: screen.height - 35, width: 200, height: 15)
        self.fudgeDateTag.font = UIFont(name: "Helvetica-Bold", size: 10)
        self.fudgeDateTag.text = "Fecha de expriación"
        self.fudgeDateTag.textColor = UIColor.white
        self.fudgeDateTag.textAlignment = .center
        self.fudgeDateTag.center.x = screen.width / 2

        retryButton.addTarget(self, action: #selector(self.editFudge), for: .touchUpInside)
        doneButton.addTarget(self, action: #selector(self.endProcess), for: .touchUpInside)


            }



    override func viewDidAppear(_ animated: Bool) {
        /* CardIOView *cardIOView = [[CardIOView alloc] initWithFrame:CGRECT_WITHIN_YOUR_VIEW];
         cardIOView.delegate = self;

         [self.view addSubview:cardIOView];
         */
        aCardIOView.frame = self.view.frame
        aCardIOView.delegate = self
        aCardIOView.hideCardIOLogo = true
        self.view.addSubview(aCardIOView)
        self.view.addSubview(fudgeTag)
        self.view.addSubview(fudgeDateTag)
        self.view.addSubview(doneButton)
        self.view.addSubview(fudgeLabel)
        self.view.addSubview(fudgeDate)
        self.view.addSubview(retryButton)
        self.view.addSubview(navbar)


    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
