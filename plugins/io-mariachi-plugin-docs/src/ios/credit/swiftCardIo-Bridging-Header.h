//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

#import <Cordova/CDV.h>
#import "CardIO.h"
#import "CardIOCreditCardInfo.h"
#import "CardIODetectionMode.h"
#import "CardIOPaymentViewController.h"
#import "CardIOPaymentViewControllerDelegate.h"
#import "CardIOUtilities.h"
#import "CardIOView.h"
#import "CardIOViewDelegate.h"


// $(inherited) -ObjC -l"CardIO" -l"c++" -l"opencv_core" -l"opencv_imgproc" -framework "AVFoundation" -framework "Accelerate" -framework "AudioToolbox" -framework "CoreMedia" -framework "CoreVideo" -framework "MobileCoreServices" -framework "OpenGLES" -framework "QuartzCore" -framework "Security" -framework "UIKit"
